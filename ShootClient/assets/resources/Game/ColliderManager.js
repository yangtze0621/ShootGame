// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        debugDraw:true,
        graphics : cc.Graphics
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.friendGroup = [];
        this.neutralGroup = [];
        this.enemyGroup = [];
        this.wallGroup = []
        
        this.a_friendBulletGroup = [];
        this.a_neutralBulletGroup = [];
        this.a_enemyBulletGroup = [];
        if(this.debugDraw && !this.graphics){
            cc.warn('没有绘制组件')
            this.debugDraw = false;
            this.graphics.node.active = true;
        } else if(!this.debugDraw && this.graphics){
            this.graphics.node.active = false;
        }
    },

    addUnit(script){
        if(script.camp == undefined || script.camp == null){
            return cc.error('没有阵容，无法添加')
        }
        if(script.camp == global.Enum.CAMP.neutral){
            this.neutralGroup.push(script);
        } else if(script.camp == global.Enum.CAMP.friend){
            this.friendGroup.push(script);
        }else if(script.camp == global.Enum.CAMP.enemy){
            this.enemyGroup.push(script);
        } else if(script.camp == global.Enum.CAMP.wall){
            this.wallGroup.push(script);
        }
    },

    removeArrayChild(arr,child){
        var i =0;
        while(i<arr.length){
            if(arr[i] == child){
                arr.splice(i,1);
            } else {
                i++
            }
        }
    },

    removeUnit(script){
        if(script.camp == global.Enum.CAMP.neutral){
            this.removeArrayChild(this.neutralGroup,script)
        } else if(script.camp == global.Enum.CAMP.friend){
            this.removeArrayChild(this.friendGroup,script)
        }else if(script.camp == global.Enum.CAMP.enemy){
            this.removeArrayChild(this.enemyGroup,script)
        }else if(script.camp == global.Enum.CAMP.wall){
            this.removeArrayChild(this.wallGroup,script)
        }
    },

    addBullet(script){
        if(typeof(script.camp) == 'undefined'){
            return cc.error('没有阵容，无法添加')
        }
        this.removeUnit(script);
        if(script.camp == global.Enum.CAMP.neutral){
            this.a_neutralBulletGroup.push(script);
        } else if(script.camp == global.Enum.CAMP.friend){
            this.a_friendBulletGroup.push(script);
        }else if(script.camp == global.Enum.CAMP.enemy){
            this.a_enemyBulletGroup.push(script);
        }
    },
    removeBullet(script){
        if(script.camp == global.Enum.CAMP.neutral){
            this.removeArrayChild(this.a_neutralBulletGroup,script)
        } else if(script.camp == global.Enum.CAMP.friend){
            this.removeArrayChild(this.a_friendBulletGroup,script)
        }else if(script.camp == global.Enum.CAMP.enemy){
            this.removeArrayChild(this.a_enemyBulletGroup,script)
        }
    },

    getBoundingBoxToWorld(_js){
        var no = _js.node
        var bulletRect;
        if(_js.rect){ //如果有指定的大小，则不定
            var start = no.convertToWorldSpace(cc.v2(no.width*no.anchorX,no.height*no.anchorY))
            var rect = _js.rect
            bulletRect = cc.rect(start.x-cc.winSize.width/2+rect.x,start.y-cc.winSize.height/2+rect.y,rect.width,rect.height)
        } else {
            var start = no.convertToWorldSpace(cc.v2(no.width*no.anchorX,no.height*no.anchorY))
            bulletRect = cc.rect(start.x-cc.winSize.width/2-no.width*no.anchorX,start.y-cc.winSize.height/2-no.width*no.anchorY,no.width,no.height)
        }
        return bulletRect
    },


    checkCollider(bullets,group,ignore=true){
        for(var i=0; i<bullets.length; i++){
            let blt = bullets[i];
            let bulletRect = this.getBoundingBoxToWorld(blt);
            for(var j=0; j<group.length; j++){
                if(ignore&&group[j].isIgnore){
                    continue;
                }
                let other = this.getBoundingBoxToWorld(group[j]);
                if(blt.isValid && bulletRect.intersects(other)){
                    if(!blt){
                        cc.warn('not found onColliderEnter',blt.node.name)
                    }
                    var c = blt.onColliderEnter(group[j]) //碰撞后只通知子弹，由子弹来检测碰撞
                    if(c){
                        return;
                    }
                }
            }
        }
    },

    checkMove(bulletRect,box){
        var self = this;
        function checkGroup(group){
            for(var j=0; j<group.length; j++){
                if(group[j].isIgnore){
                    continue;
                }
                let other = self.getBoundingBoxToWorld(group[j]);
                if(bulletRect.intersects(other)){
                    return group[j]
                }
            }
            return null;
        }
        return checkGroup(this.neutralGroup) || checkGroup(this.wallGroup);
    },

    drawRect(arr,scale=1){
        for(var i=0; i<arr.length; i++){
            var bulletRect = this.getBoundingBoxToWorld(arr[i])
            this.graphics.rect(bulletRect.xMin,bulletRect.yMin,bulletRect.size.width,bulletRect.size.height);
        }
    },



    gameUpdate(dt){
        this.checkCollider(this.a_friendBulletGroup,this.wallGroup);
        this.checkCollider(this.a_friendBulletGroup,this.neutralGroup);

        if(!this.debugDraw){
            return;
        }

        this.graphics.clear();
        this.graphics.strokeColor = cc.color(0,255,0,255);
        this.graphics.lineWidth = 2;
        this.drawRect(this.friendGroup);
        this.drawRect(this.neutralGroup);
        this.drawRect(this.wallGroup);

        this.drawRect(this.a_friendBulletGroup);
        // this.drawRect(this.a_enemyBulletGroup);
        this.graphics.stroke();
    }

});
