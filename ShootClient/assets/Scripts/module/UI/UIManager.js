/* UIManager的逻辑是
如果有新的UI添加在上面，则会先隐藏下面那层
上面一层删除了，然后再逐步显示
*/
cc.Class({
    extends: require('Component'),

    properties: {
    },

    onLoad(){
        this.uiWindowNames = []; // 弹出的窗口
        this.uiWindows = [];
        this.isEjectAnimate = false;
    },

    clearNode(){
        this.node.destroyAllChildren();
        this.uiWindowNames = []; 
        this.uiWindows = [];
    },

    addUI(name){ //强行添加上去的层，跳过pop的处理
        var node = global.Loader.getInstantiate(name)
        this.node.addChild(node);
        return node;
    },

    pushUI(name,script,options){
        this.uiWindowNames.push({name:name,script:script,options:options});
        return this._updateWindow()
    },
    postFocus(){
        var endLast = this.uiWindows.length > 0 ? this.uiWindows[this.uiWindows.length-1] :null;
        if(endLast){
            endLast.emit('onWindowFocus');
        }
    },

    popUI(){
        var end = this.uiWindows.length > 0 ? this.uiWindows[this.uiWindows.length-1] :null;
        if(!end){
            cc.warn('都没有，还移除什么');
            return 
        }
        this.uiWindowNames.pop();
        var self = this;
        end.emit('onExitWindowBegin');
        this.isEjectAnimate = true;
        this.node.runAction(cc.sequence(cc.delayTime(0.5),cc.callFunc(function(){
            end.emit('onExitWindowEnd');
            end.destroy();
            self.isEjectAnimate = false;
            self._updateWindow()
        })))
        this.uiWindows.pop();
        var endLast = this.uiWindows.length > 0 ? this.uiWindows[this.uiWindows.length-1] :null;
        if(endLast){
            endLast.emit('onEnterWindowBegin');
            endLast.emit('onWindowFocus');
        }
    },

    _updateWindow(){
        if(this.isEjectAnimate || this.uiWindowNames.length == 0){
            return null;
        }
        var base = this.uiWindowNames.pop();
        this.isEjectAnimate = true;
        var node = global.Loader.getInstantiate(base.name)
        this.node.addChild(node);
        this._addUIAnimate(node)
        if(base.script && typeof(base.script) == 'string'){
            if(base.options){
                node.getComponent(base.script).setOptions(base.options)
            }
        } else if(typeof(base.script) == 'object'){
            var _js = base.name.substring(base.name.lastIndexOf('/')+1)
            node.getComponent(_js).setOptions(base.script)
        }
        
        return node;
    },

    _addUIAnimate(node){
        var self = this;
        var last = null;
        if(this.uiWindows.length > 0){
            last = this.uiWindows[this.uiWindows.length-1];
        }
        if(last){
            if(!last._uimgrloadEnd){
                if(this.enteraction){
                    this.unschedule(this.enteraction)
                    this.enteraction = null;
                }
                last.emit('onExitWindowBegin');
                self.isEjectAnimate = false;
            } else {
                last.emit('onExitWindowBegin');
                this.node.runAction(cc.sequence(cc.delayTime(0.5),cc.callFunc(function(){
                    last['onExitWindowEnd'] && last['onExitWindowEnd']();
                    last._uimgrloadEnd = true;
                    self.isEjectAnimate = false;
                })))
            }
            
        } else {
            this.isEjectAnimate = false;
        }
        this.enteraction = this.scheduleOnce(()=>{
            node.emit('onEnterWindowBegin');
            self.enteraction = null;
        },0.01)
        // setTimeout(()=>{ //延时一帧，保证所有方法都初始化完成
        //     node.emit('onEnterWindowBegin');
        // },10)
        self.uiWindows.push(node);
        this._updateWindow();
    },

    //添加动态创建的层layerid可以为数组下标，也可以为完整的prefab路径
    //如果options，存在，则逻辑为  取js的脚本传options的值  比如 ('Chat','UI/chat/ChatLayer','ChatMenu',3) 逻辑为 node.getComponent('ChatMenu').setOptions(3)
    //如果options不存在，则默认取layer的最后的名字传值js   比如 ('Chat','UI/chat/ChatLayer',1) 逻辑为 node.getComponent('ChatLayer').setOptions(1)
    loadLayer(groupName,layerId,js,options){
        var arr = global.Resources[groupName];
        var layer = global.Common.isNumber(layerId) ? arr[layerId] : layerId
        var _js = options != undefined ? js : layer.substring(layer.lastIndexOf('/')+1)
        var _opt = options == undefined ? js : options
        global.Loader.loadResources(groupName,function(err,res){
            var chat = global.UIMgr.pushUI(layer,_js,_opt);
            // if(_opt){
            //     chat.getComponent(_js).setOptions(_opt);
            // }
        })
    }
});
